require 'rspec'
require_relative '../hello'

describe Hello do
  it 'returns "Hello World!"' do
    expect(Hello.new.say).to eq("Hello World!")
  end
end
